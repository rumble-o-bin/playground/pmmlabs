ENV["LC_ALL"] = "en_US.UTF-8"

COUNT = 1

VCPU = "2"
VMEM = "2048"
NETWORK = "192.168.60.9"

# init script
$init_script = <<SCRIPT
    # sudo sed -i "s*archive.ubuntu.com*mirror.0x.sg*g" /etc/apt/sources.list
    sudo apt update --fix-missing
SCRIPT

# install docker
$install_docker = <<SCRIPT
    sudo apt-get install -y \
        ca-certificates \
        curl \
        gnupg
    sudo mkdir -m 0755 -p /etc/apt/keyrings
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    echo \
        "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
        "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
    sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    sudo apt update
    sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin -y
SCRIPT

# install mysql server and client
$install_mysql = <<SCRIPT
    sudo apt install mysql-server mysql-client -y
SCRIPT

Vagrant.configure("2") do |config|
    config.vm.box = "ubuntu/focal64"
    config.vm.synced_folder "./", "/vagrant"

    (1..COUNT).each do |i|
        config.vm.define "machine-#{i}" do |node|
            node.vm.hostname = "machine-#{i}"
            node.vm.provider "virtualbox" do |v|
                v.cpus = VCPU
                v.memory = VMEM
            end
            node.vm.network :private_network, ip: "#{NETWORK}#{i}", auto_config: true
            node.vm.provision "shell", inline: $init_script
            node.vm.provision "shell", inline: $install_docker
            node.vm.provision "shell", inline: $install_mysql
        end
    end
end