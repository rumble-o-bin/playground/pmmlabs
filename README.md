Vagrant PMM Labs
---

Simple POC-ing PMM with TLS enabled

## Run

```bash
vagrant up
```

## Table of content

[[_TOC_]]

## Steps setup

### Setup DB

```bash
# Sources: https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-20-04#step-2-configuring-mysql

# init login
sudo mysql

# set root pass w/ alter
ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY '<root_db_password>';

# to using sudo in front eg: sudo mysql -u xxxx
ALTER USER 'root'@'localhost' IDENTIFIED WITH auth_socket;

# exit :)
exit
```

### Setup PMM Server

```bash
docker compose -f /vagrant/hack/docker-compose.yml up -d
```

### Generate Selfsign Cert

```bash
# make sure read the script before execute it
bash /vagrant/hack/selfsign.sh

# copy to pmm-server
docker cp certificate.crt pmm-server:/srv/nginx/certificate.crt
docker cp certificate.key pmm-server:/srv/nginx/certificate.key
docker cp ca-certs.pem pmm-server:/srv/nginx/ca-certs.pem
docker cp dhparam.pem pmm-server:/srv/nginx/dhparam.pem

# dont forget to restart
docker restart pmm-server
```

### Copy ca-certs to each agent and server (make it trust)

```
cp ca-certs.pem /etc/ssl/certs/

# Then
update-ca-certificates
```

### Setup PMM agent/client

```bash
# get package
wget https://repo.percona.com/apt/percona-release_latest.generic_all.deb
dpkg -i percona-release_latest.generic_all.deb

# install agent/client
apt update
apt install -y pmm2-client

# example register
# for pmm_password you can try access the dashboard first
# default is admin:admin
pmm-admin config --server-url=https://admin:<pmm_password>@192.168.60.91:1337
```

### Add MySQL

```bash
# create user for pmm first and grant the permission
CREATE USER 'pmm'@'localhost' IDENTIFIED BY '<pmm_user_db_password>' WITH MAX_USER_CONNECTIONS 10;
GRANT SELECT, PROCESS, REPLICATION CLIENT, RELOAD, BACKUP_ADMIN ON *.* TO 'pmm'@'localhost';

# register mysql agent
pmm-admin add mysql --username=pmm --password=<pmm_user_db_password> --service-name=<DB_MACHINE_NAME> --host=127.0.0.1 --port=3306
```

## Sources

- https://docs.percona.com/percona-monitoring-and-management/index.html