#!/bin/bash

# Set the IP address of the PMM server
pmm_server_ip=192.168.60.91

# Create a private key for the PMM server
openssl genrsa -out certificate.key 2048

# Create a certificate signing request (CSR) for the PMM server
openssl req -new -key certificate.key -out certificate.csr -subj "/CN=pmm-server"

# Create a configuration file for the IP SAN and X.509v3 extension
echo subjectAltName=IP:$pmm_server_ip > pmm_server_extfile.cnf
echo -e "extensions = v3_req\n[ v3_req ]\nsubjectAltName=IP:$pmm_server_ip" >> pmm_server_extfile.cnf

# Sign the CSR to create a self-signed certificate
openssl x509 -req -days 365 -in certificate.csr -signkey certificate.key -out certificate.crt -extfile pmm_server_extfile.cnf -extensions v3_req

# Create a CA bundle file
cat certificate.crt >> ca-certs.pem

# Create a Diffie-Hellman parameter file
openssl dhparam -out dhparam.pem 2048

# Verify the certificate
openssl x509 -noout -text -in certificate.crt